
# Copyright 2020 Adam Campbell, Seth Hall, Andrew Ensor
# Copyright 2020 High Performance Computing Research Laboratory, Auckland University of Technology (AUT)

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:

# 1. Redistributions of source code must retain the above copyright notice,
# this list of conditions and the following disclaimer.

# 2. Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution.

# 3. Neither the name of the copyright holder nor the names of its
# contributors may be used to endorse or promote products derived from this
# software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import time
import json

import pytest
import numpy as np
import numpy.testing as test

import metric_generator as gen

class TestMetricGenerator():

    def setup_method(self):
        
        self.num_channels = 4
        self.num_baselines = 10
        self.num_pols = 4
        self.dimensions = (self.num_channels, self.num_baselines, self.num_pols)
        self.num_heaps_to_simulate = 5

        self.generator = gen.MetricGenerator(self.num_channels, self.num_baselines, self.num_pols)
        self.base_data = np.zeros(self.dimensions, dtype=np.complex128)

        # All channels
        self.channel_filter = np.zeros((self.num_channels, 2), dtype=np.uint32)
        for cf in range(self.num_channels):
            self.channel_filter[cf] = (cf, cf + 1)
        # All baselines  
        self.baseline_filter = np.arange(0, self.num_baselines)
        # All polarizations
        self.pol_filter = np.arange(0, self.num_pols)
        
        for x, y, z in np.ndindex(self.dimensions):
            self.base_data[x][y][z] = (x + 1) + ((y * 0.1) + (z * 0.01)) * 1j

    # Used to test YAML passing with a database connection
    def test_yaml_file_parsing_with_database_connectivity(self):
        assert self.generator.parse_yaml_output_config("test_data/test_existing_yaml_database_connection.yaml")

    # Used to test YAML passing with a new file output path
    def test_data_yaml_file_parsing_with_file_options(self):
        assert self.generator.parse_yaml_output_config("test_data/test_existing_yaml.yaml")

    def test_data_db_connection_failed_missing_attributes(self):
        assert not self.generator.setup_db_connection({})

    # Used to make sure metric generator handles a non existing YAML config file 
    def test_non_existing_yaml(self):
        with pytest.raises(ValueError):
            self.generator.parse_yaml_output_config("non_existing_yaml.yaml")

    # Testing the overall mean of real components
    # Channels: All
    # Baselines: All
    # Polarizations: All
    def test_real_mean_all_channel_baseline_pol(self):

        # Feed new metric instance to generator
        self.generator.add_new_metric("unit_test", self.channel_filter, self.baseline_filter, 
            self.pol_filter, "real", "mean", self.num_heaps_to_simulate)

        # Simulate accumulation of data
        for _ in range(self.num_heaps_to_simulate+1):
            self.generator.process_payload(self.base_data, 0, self.num_channels, time.time())

        actual = gen.MetricGenerator.load_metrics_from_file("test_data/test_data_real_mean.txt", self.dimensions)
        expected = "test_data/metric_output_id-unit_test_operand-real_operator-mean.txt"
        calculated = gen.MetricGenerator.load_metrics_from_file(expected, self.dimensions)
        test.assert_allclose(actual, calculated)

    # Testing the variance of imaginary components
    # Channels: ((0, 2), (2, 4))
    # Baselines: All
    # Polarizations: All
    def test_imag_variance_all_baseline_pol_subset_channels(self):
                
        # Subset channels
        self.channel_filter = np.array([(0, 2), (2, 4)])
        # Feed new metric instance to generator
        self.generator.add_new_metric("unit_test", self.channel_filter, self.baseline_filter, 
            self.pol_filter, "imaginary", "variance", self.num_heaps_to_simulate)

        # Simulate accumulation of data
        for _ in range(self.num_heaps_to_simulate+1):
            self.generator.process_payload(self.base_data, 0, self.num_channels, time.time())

        # Compressed channels, all baselines, all pols
        actual_dims = (self.channel_filter.shape[0], self.dimensions[1], self.dimensions[2])
        actual = gen.MetricGenerator.load_metrics_from_file("test_data/test_data_imaginary_variance.txt", actual_dims)
        expected = "test_data/metric_output_id-unit_test_operand-imaginary_operator-variance.txt"
        calculated = gen.MetricGenerator.load_metrics_from_file(expected, actual_dims)
        test.assert_allclose(actual, calculated)

    # Testing the min of calculated amplitude
    # Channels: All
    # Baselines: (0, 2)
    # Polarizations: All
    def test_amplitude_min_all_channel_pol_subset_baseline(self):

        # Subset baselines
        self.baseline_filter = np.arange(0, self.num_baselines, step=2)
        # Feed new metric instance to generator
        self.generator.add_new_metric("unit_test", self.channel_filter, self.baseline_filter, 
            self.pol_filter, "amplitude", "min", self.num_heaps_to_simulate)

        # Simulate accumulation of data
        for _ in range(self.num_heaps_to_simulate+1):
            self.generator.process_payload(self.base_data, 0, self.num_channels, time.time())

        # All channels, subset baselines, all pols
        actual_dims = (self.dimensions[0], self.baseline_filter.shape[0], self.dimensions[2])
        actual = gen.MetricGenerator.load_metrics_from_file("test_data/test_data_amplitude_min.txt", actual_dims)
        expected = "test_data/metric_output_id-unit_test_operand-amplitude_operator-min.txt"
        calculated = gen.MetricGenerator.load_metrics_from_file(expected, actual_dims)
        test.assert_allclose(actual, calculated)

    # Testing the max of calculated phase
    # Channels: All
    # Baselines: All
    # Polarizations: (0, 2)
    def test_phase_max_all_channel_baseline_subset_pol(self):
        
        # Subset polarizations
        self.pol_filter = np.array([0, 2])
        # Feed new metric instance to generator
        self.generator.add_new_metric("unit_test", self.channel_filter, self.baseline_filter, 
            self.pol_filter, "phase", "max", self.num_heaps_to_simulate)

        # Simulate accumulation of data
        for _ in range(self.num_heaps_to_simulate+1):
            self.generator.process_payload(self.base_data, 0, self.num_channels, time.time())

        # All channels, all baselines, subset pols
        actual_dims = (self.dimensions[0], self.dimensions[1], self.pol_filter.shape[0])
        actual = gen.MetricGenerator.load_metrics_from_file("test_data/test_data_phase_max.txt", actual_dims)
        expected = "test_data/metric_output_id-unit_test_operand-phase_operator-max.txt"
        calculated = gen.MetricGenerator.load_metrics_from_file(expected, actual_dims)
        test.assert_allclose(actual, calculated)

    # Validates that metric was added to queue
    def test_metric_exists(self):
        self.generator.add_new_metric("unit_test", self.channel_filter, self.baseline_filter, 
            self.pol_filter, "real", "mean", self.num_heaps_to_simulate)
        assert self.generator.does_metric_exist("unit_test")

    # Validates that no metric titled 'other' exists
    def test_metric_doesnt_exist(self):
        assert not self.generator.does_metric_exist("other")

    # Validates that metrics with the same identifier cannot replace one another
    def test_add_metric_already_exists(self):
        self.generator.add_new_metric("unit_test", self.channel_filter, self.baseline_filter, 
            self.pol_filter, "real", "mean", self.num_heaps_to_simulate)
        assert not self.generator.add_new_metric("unit_test", self.channel_filter, self.baseline_filter, 
            self.pol_filter, "real", "mean", self.num_heaps_to_simulate)

    # Validates that updating a metric is successful
    def test_update_metric_already_exists(self):
        self.generator.add_new_metric("unit_test", self.channel_filter, self.baseline_filter, 
            self.pol_filter, "real", "mean", self.num_heaps_to_simulate)
        assert self.generator.update_existing_metric("unit_test")
        
    # Validates that a non-existant metric cannot be updated
    def test_update_metric_doesnt_exist(self):
        assert not self.generator.update_existing_metric("unit_test")

    # Validates that when no metrics present, an empty list is returned
    def test_get_all_metrics_no_metrics(self):
        metrics = self.generator.get_list_of_metrics()
        assert metrics

    # Validates that when a metric exists, it can be fetched and queried for attributes
    def test_get_all_metrics_existing_metric(self):
        self.generator.add_new_metric("unit_test", self.channel_filter, self.baseline_filter, 
            self.pol_filter, "real", "mean", self.num_heaps_to_simulate)
        test_metric = json.loads(self.generator.get_list_of_metrics())[0]
        assert test_metric["operand"] == "real"
        assert test_metric["operator"] == "mean"

    # Validates that request a non-existant metric results in a no data response
    def test_get_metric_config_no_metric(self):
        assert self.generator.get_metric_config("unit_test") == "null"

    # validates that an existing metric can be queried for, and data validated as identical
    def test_get_metric_config_existing_metric(self):
        self.generator.add_new_metric("unit_test", self.channel_filter, self.baseline_filter, 
            self.pol_filter, "real", "mean", self.num_heaps_to_simulate)
        metric = json.loads(self.generator.get_metric_config("unit_test"))
        print(metric)
        chan_fil = np.array(metric["channel_filters"])
        baseline_fil = np.array(metric["baseline_filters"])
        pol_fil = np.array(metric["pol_filters"])
        test.assert_array_equal(self.channel_filter, chan_fil)
        test.assert_array_equal(self.baseline_filter, baseline_fil)
        test.assert_array_equal(self.pol_filter, pol_fil)

    # Validates that a metric cannot be added when attempted with invalid operand/operator options
    def test_add_metric_invalid_operator_and_operand(self):
        with pytest.raises(ValueError):
            self.generator.add_new_metric("unit_test", self.channel_filter, self.baseline_filter, 
                    self.pol_filter, "hello", "world", self.num_heaps_to_simulate)

    def test_add_metric_invalid_filters(self):
        with pytest.raises(ValueError):
            self.generator.add_new_metric("unit_test", None, None,  None, 
                "real", "mean", self.num_heaps_to_simulate)

    def test_add_metric_invalid_identifier(self):
        with pytest.raises(ValueError):
            self.generator.add_new_metric(None, self.channel_filter, self.baseline_filter, 
                    self.pol_filter, "real", "mean", self.num_heaps_to_simulate)

    # Validates that only an existing metric can be removed from the generator
    def test_remove_metric_exists(self):
        self.generator.add_new_metric("unit_test", self.channel_filter, self.baseline_filter, 
            self.pol_filter, "real", "mean", self.num_heaps_to_simulate)
        assert self.generator.does_metric_exist("unit_test")
        self.generator.remove_existing_metric("unit_test")
        assert not self.generator.does_metric_exist("unit_test")

    # Validates that a non-existing metric cannot be removed
    def test_remove_metric_doesnt_exist(self):
        assert not self.generator.remove_existing_metric("unit_test")

    # Validates that a metric with an expired end-timestamp will be removed from the generator automatically
    def test_metric_already_expired(self):
        # Reduce end_timestamp to one hour ago
        self.generator.add_new_metric("unit_test", self.channel_filter, self.baseline_filter, 
            self.pol_filter, "real", "mean", self.num_heaps_to_simulate, end_timestamp = time.time() - (60 * 60))
        # Simulate processing (will destroy metric due to timestamp expiration)
        self.generator.process_payload(self.base_data, 0, self.num_channels, time.time())
        # No longer queued for processing
        assert not self.generator.does_metric_exist("unit_test")

    def test_metric_pol_strings(self):
        m = self.generator.Metric("id", np.zeros((1, 2)), np.arange(0, 1), np.arange(0, 4), "real", "mean", 0)
        assert m.get_pol_string(0) == 'XX'
        assert m.get_pol_string(1) == 'XY'
        assert m.get_pol_string(2) == 'YX'
        assert m.get_pol_string(3) == 'YY'
        assert m.get_pol_string(4) == 'unknown'
        with pytest.raises(ValueError):
            m.get_pol_string(None)

    def test_metric_generator_invalid_params(self):
        with pytest.raises(ValueError):
            gen.MetricGenerator(None, 0, None)

    def test_get_metric_operand_no_payload(self):
        with pytest.raises(ValueError):
            m = self.generator.Metric("id", np.zeros((1, 2)), np.arange(0, 1), np.arange(0, 4), "real", "mean", 0)
            m.get_metric_operand(None)

    def test_perform_metric_op_no_payload(self):
        with pytest.raises(ValueError):
            m = self.generator.Metric("id", np.zeros((1, 2)), np.arange(0, 1), np.arange(0, 4), "real", "mean", 0)
            m.perform_metric_op(None, None)

    def test_process_payload_no_payload(self):
        with pytest.raises(ValueError):
            m = self.generator.Metric("id", np.zeros((1, 2)), np.arange(0, 1), np.arange(0, 4), "real", "mean", 0)
            m.process_payload(None, 0, 1)

    def test_process_payload_invalid_channel_range(self):
        m = self.generator.Metric("id", np.zeros((1, 2)), np.arange(0, 1), np.arange(0, 4), "real", "mean", 0)
        with pytest.raises(ValueError):
            m.process_payload(self.base_data, 100, 10)

    def test_new_metric_invalid_identifier(self):
        with pytest.raises(ValueError):
            self.generator.Metric("", np.zeros((1, 2)), np.arange(0, 1), np.arange(0, 4), "real", "mean", 0)

    def test_new_metric_invalid_filter(self):
        with pytest.raises(ValueError):
            self.generator.Metric("id", None, np.arange(0, 1), np.arange(0, 4), "real", "mean", 0)

    def test_new_metric_invalid_operand_operator(self):
        with pytest.raises(ValueError):
            self.generator.Metric("id", np.zeros((1, 2)), np.arange(0, 1), np.arange(0, 4), "hello", "world", 0)

    def test_load_metric_file_invalid_filename(self):
        with pytest.raises(ValueError):
            gen.MetricGenerator.load_metrics_from_file("hello_world.txt", (0, 1, 2))

    def test_load_metric_file_invalid_dimensions(self):
        with pytest.raises(ValueError):
            gen.MetricGenerator.load_metrics_from_file("test_data/test_data_real_mean.txt", None)

    def test_process_payload_missing_payload(self):
        with pytest.raises(ValueError):
            self.generator.process_payload(None, 0, 1, time.time())

    def test_process_payload_missing_timestamp(self):
        with pytest.raises(ValueError):
            self.generator.process_payload(self.base_data, 0, 1, None)

    def test_dispatch_metric_to_file_no_metric(self):
        with pytest.raises(ValueError):
            self.generator.dispatch_metric_file(None)

    def test_dispatch_metric_to_db_no_metric(self):
        with pytest.raises(ValueError):
            self.generator.dispatch_metric_db(None)