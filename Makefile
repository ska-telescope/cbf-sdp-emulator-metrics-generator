# Set dir of Makefile to a variable to use later
MAKEPATH := $(abspath $(lastword $(MAKEFILE_LIST)))
BASEDIR := $(patsubst %/,%,$(dir $(MAKEPATH)))

# Cross platform make declarations
# \
!ifndef 0 # \
# nmake code here \
MKDIR=mkdir # \
RMRF=del /f /s /q # \
!else
# make code here
MKDIR=mkdir -p
RMRF=rm -rf
# \
!endif

NAME := cbf-sdp-emulator-metric-generator
# Data processing params
DATASET_FOLDER=datasets
DATASET=sim-vis.ms
DATABASE_NAME=gleam
HOSTNAME=localhost # IP of where influx installed
PORT_NUMBER=8086
TIMESTEPS=133
CHANNELS_PER_STREAM=4
SENDER=metric_generator_send
RECEIVER=metric_generator_recv

# define your personal overides for above variables in here
-include PrivateRules.mak

.PHONY: vars help test k8s show lint deploy delete logs describe namespace default all clean
.DEFAULT_GOAL := help

# Set dir of Makefile to a variable to use later
MAKEPATH := $(abspath $(lastword $(MAKEFILE_LIST)))
BASEDIR := $(patsubst %/,%,$(dir $(MAKEPATH)))

image: ## Builds the Docker image
	docker build -t $(NAME):latest -f Dockerfile .

send-demo: ## Runs send demo for integration between metric generator and cbd-sdp-emulator
	docker run -v $(BASEDIR)/$(DATASET_FOLDER):/$(DATASET_FOLDER) \
	  --rm --name $(SENDER) \
	  --link $(RECEIVER):$(RECEIVER) $(NAME) \
	  emu-send /$(DATASET_FOLDER)/$(DATASET) -o \
	  transmission.channels_per_stream=$(CHANNELS_PER_STREAM) -o \
	  transmission.target_host=$(RECEIVER)

recv-demo: ## Runs recv demo for integration between metric generator and cbd-sdp-emulator 
	docker run -v $(BASEDIR)/$(DATASET_FOLDER):/$(DATASET_FOLDER) \
	--rm --name $(RECEIVER) $(NAME) \
	emu-recv -o \
	reception.datamodel=/$(DATASET_FOLDER)/$(DATASET) -o \
	reception.consumer=cbf_sdp_consumer.consumer -o \
	transmission.channels_per_stream=$(CHANNELS_PER_STREAM) -o \
	reception.ring_heaps=$(TIMESTEPS) -o \
	metrics_db_connection.database_name=$(DATABASE_NAME) -o \
	metrics_db_connection.host_name=$(HOSTNAME) -o \
	metrics_db_connection.port_number=$(PORT_NUMBER)

help:  ## show this help.
	@echo "$(MAKE) targets:"
	@grep -E '^[0-9a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ": .*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
	@echo ""; echo "make vars (+defaults):"
	@grep -E '^[0-9a-zA-Z_-]+ \?=.*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = " \\?\\= | ## "}; {printf "\033[36m%-30s\033[0m %-20s %-30s\n", $$1, $$2, $$3}'
