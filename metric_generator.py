"""
# Copyright 2020 Adam Campbell, Seth Hall, Andrew Ensor
# Copyright 2020 High Performance Computing Research Laboratory
# Copyright 2020 Auckland University of Technology (AUT)

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:

# 1. Redistributions of source code must retain the above copyright notice,
# this list of conditions and the following disclaimer.

# 2. Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution.

# 3. Neither the name of the copyright holder nor the names of its
# contributors may be used to endorse or promote products derived from this
# software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
"""

import os.path
from datetime import datetime as dt, timezone
import time
import json
import warnings
import logging as log

import yaml
import numpy as np
from influxdb import InfluxDBClient
from influxdb.exceptions import InfluxDBClientError, InfluxDBServerError

# pylint: disable=too-many-locals
# pylint: disable=too-many-arguments
# pylint: disable=too-many-instance-attributes
# pylint: disable=attribute-defined-outside-init
# pylint: disable=logging-fstring-interpolation

# To be changed: put this in a more suitable place...
VALID_OPERANDS = ["real", "imaginary", "amplitude", "phase"]
VALID_OPERATORS = ["min", "max", "mean", "variance"]


class MetricGenerator:
    """
    An object which "drives" the management of metric instances(ie: add,
    remove, update, query of metrics). Manages processing of payloads
    (ie: 3D numpy arrays) across collection of active metric instances.
    Dispatchs and removes completed metrics to flatfile/database
    (depending on config settings).
    """

    def __init__(self, num_channels, num_baselines, num_pols) -> None:

        args = (num_channels, num_baselines, num_pols)
        if any(arg is None or arg <= 0 for arg in args):
            raise ValueError("Number of Channel/Baseline/Pols cannot be "
                             "negative or zero")

        self.metric_dict = {}
        self.num_channels = num_channels
        self.num_baselines = num_baselines
        self.num_pols = num_pols
        self.to_database = False
        self.database = None
        self.output_file_path = "test_data/metric_output_"
        self.database = "localhost"
        self.client = None

    def parse_yaml_output_config(self, yaml_file) -> bool:
        """
        used to configure metric data outputs using a YAML config file.
        This method is optional if a non default output file path should be set
        or a influx time series database should instead be used
        for metric outputs
        """
        if yaml_file is None or not os.path.isfile(yaml_file):
            raise ValueError("Specified YAML file does not exist: "
                             f"{yaml_file}")

        with open(yaml_file, "r") as file:
            yaml_dict = yaml.load(file)

        if "output_to_database" in yaml_dict:
            self.to_database = yaml_dict.get("output_to_database")
            if self.to_database:
                success = self.setup_db_connection(yaml_dict)
                if not success:
                    warnings.warn("Database connection not established "
                                  "as potentially invalid parameters "
                                  "specified in YAML configuration file.")
                    self.to_database = False
                    return False
                return True
        else:
            warnings.warn("Unable to determine 'use database' flag from "
                          "YAML configuration file. Defaulting to "
                          "flatfiles.")

        if "output_file_path" in yaml_dict:
            self.output_file_path = yaml_dict.get("output_file_path")
            log.info("New flatfile output path interpreted from YAML "
                     "configuration file.")

        return True

    def setup_db_connection(self, yaml_dict) -> bool:
        """
        Helper method to test and setup a influx timeseries database,
        needs specifc config strings set in YAML doc
        """
        if not {"database_name","host_name","port_number"} <= yaml_dict.keys():
            return False

        port = yaml_dict.get("port_number")
        host = yaml_dict.get("host_name")
        self.database = yaml_dict.get("database_name")

        try:
            self.client = InfluxDBClient(
                host, port = port,
                database = self.database
            )
        except (InfluxDBServerError, InfluxDBClientError) as influx_error:
            warnings.warn(
                "Unable to connect to InfluxDB. Connection "
                "params are potentially invalid or InfluxDB "
                f"service may be down. From InfluxDB: {influx_error}"
            )
            return False

        return True

    def add_new_metric(
            self, identifier, channel_filter, baseline_filter,
            pol_filter, operand, operator, num_timesteps_to_process = 1,
            start_timestamp = None, end_timestamp = None) -> bool:
        """
        Adds new instance of metric to track, assuming valid args
        """
        # Validate filter lists
        filters = (channel_filter, baseline_filter, pol_filter)
        if any(fil is None or len(fil) <= 0 for fil in filters):
            raise ValueError("Channel/Baseline/Pol filters cannot be empty")

        # Validate identifier
        if identifier is None or len(identifier) <= 0:
            raise ValueError("New metric identifier cannot be empty")

        # Validate supplied operator/operand
        ops_set = set(VALID_OPERANDS + VALID_OPERATORS)
        if not set([operator, operand]) <= ops_set:
            raise ValueError(f"Invalid operator or operand provided"
                             f"when adding new metric instance. Operator: "
                             f"{operator}, operand: {operand}.")

        # Refuse if key already in use
        if self.does_metric_exist(identifier):
            log.info(f"Attempted to add metric instance with already in use"
                     f"identifier: {identifier}.")
            return False

        # Otherwise accept
        self.metric_dict[identifier] = self.Metric(
            identifier, channel_filter, baseline_filter, pol_filter, operand,
            operator, num_timesteps_to_process, start_timestamp,
            end_timestamp
        )
        return True

    def update_existing_metric(
            self, identifier, channel_filter = None,
            baseline_filter = None, pol_filter = None,
            operand = None, operator = None,
            num_timesteps_to_process = None,
            start_timestamp = None, end_timestamp = None) -> bool:
        """
        Removes and reinstates fresh metric based on existing metric instance.
        All arguments optional as can derive from existing metric instance.
        """
        # No metric exists with this ID, ignore
        if not self.does_metric_exist(identifier):
            log.info(f"Attempted to update non-existing metric instance"
                      f"using id: {identifier}.")
            return False

        # Grab copy of old metric
        old = self.metric_dict[identifier]

        # Get rid of old metric
        self.remove_existing_metric(identifier)

        # Potential persistance of old metric attributes
        if channel_filter is None:
            channel_filter = old.channel_filter
        if baseline_filter is None:
            baseline_filter = old.baseline_filter
        if pol_filter is None:
            pol_filter = old.pol_filter
        if operand is None:
            operand = old.operand
        if operator is None:
            operator = old.operator
        if num_timesteps_to_process is None:
            num_timesteps_to_process = old.num_timesteps_to_process

        # Attempt to add for tracking
        return self.add_new_metric(
            identifier, channel_filter,
            baseline_filter, pol_filter, operand,
            operator, num_timesteps_to_process,
            start_timestamp, end_timestamp
        )

    def get_list_of_metrics(self) -> str:
        """
        Returns list of metrics with basic info in form of JSON object
        (could be changed to something else should it be required)
        """
        metrics = []
        for metric in self.metric_dict.values():
            metrics.append(metric.get_basic_metadata())
        return json.dumps(metrics)

    def get_metric_config(self, identifier) -> str:
        """
        Returns full available information about one specified metric
        """
        # Nothing useful to return, doesnt exist
        if not self.does_metric_exist(identifier):
            return json.dumps(None)
        # Otherwise we found it
        return json.dumps(self.metric_dict[identifier].get_full_metadata())

    def remove_existing_metric(self, identifier) -> bool:
        """
        Removes a metric from active payload processing
        """
        if self.does_metric_exist(identifier):
            del self.metric_dict[identifier]
            return True
        return False

    def does_metric_exist(self, identifier) -> bool:
        """
        Determines if key already exists in dict (ie: existing metric)
        """
        return identifier in self.metric_dict

    def process_payload(
            self, payload, start_channel,
            end_channel, timestep_timestamp) -> None:
        """
        Dispatches payload to each metric being tracked for processing,
        the data is saved to file or db. Maintains a list of metrics to
        be removed and dispatched after full metric list cycled.
        Temp list is used to not mess with the for loop.
        """
        if payload is None:
            raise ValueError("Payload cannot be None.")

        if timestep_timestamp is None:
            raise ValueError("Timestep_timestamp cannot be None.")

        chans = (start_channel, end_channel)
        # putting the if statement as multiline results in a syntax error.
        if any(chan is None or chan < 0 for chan in chans) or end_channel < start_channel:
            raise ValueError("Start/end channels cannot be negative, "
                             "and end channel must not be smaller than "
                             "start channel.")

        to_be_removed = []
        metrics = self.metric_dict.values()

        # Process payload across each metric
        for met in metrics:

            # Has the metric started?
            if met.start_timestamp <= time.time():

                if timestep_timestamp > met.timestep_timestamp:
                    met.timestep_timestamp = timestep_timestamp
                    met.timesteps_processed += 1

                if met.timesteps_processed > met.num_timesteps_to_process:
                    met.timesteps_processed -=1
                    if self.to_database:
                        self.dispatch_metric_db(met)
                    else:
                        self.dispatch_metric_file(met)

                    met.timestep_timestamp = timestep_timestamp
                    met.timesteps_processed += 1

                #Process payload and determine if we have processed all
                # anticipated payloads for current timestep
                met.process_payload(payload, start_channel, end_channel)

                # Mark for destruction
                if met.end_timestamp <= time.time():
                    to_be_removed.append(met.identifier)

        # Destroy any expired metric instances
        for identifier in to_be_removed:
            del self.metric_dict[identifier]

    def dispatch_metric_file(self, metric) -> None:
        """
        Sends computed metric output to file.
        Saves three-dimensional matrix into file as series of 2D matrices
        https://stackoverflow.com/a/3685339
        """
        if metric is None:
            raise ValueError("Metric cannot be None.")

        file_name = (
            f"{self.output_file_path}id-{metric.identifier}_operand-"
            f"{metric.operand}_operator-{metric.operator}.txt"
        )

        log.info(f"Dispatching metric {metric.identifier} to file {file_name}")
        metric.post_processing()

        with open(file_name, "w") as outfile:
            outfile.write(f"# Identifier: {metric.identifier}\n")
            outfile.write(f"# Data shape (channels/baselines/pols): "
                          f"{metric.metric_data.shape}\n")
            for data_slice in metric.metric_data:
                np.savetxt(outfile, data_slice, fmt="%f")
                outfile.write("# New slice\n")

        metric.reset_metric()

    def dispatch_metric_db(self, metric) -> None:
        """
        Sends the computed metric to influx DB database, the influxDB field is
        the chosen operator and influx value is the operand and channel
        number/start channel). Baseline and polarization and channel
        range are key tag values.
        """
        if metric is None:
            raise ValueError("Metric cannot be None.")

        log.info(f"Dispatching metric {metric.identifier} to InfluxDB")

        metric.post_processing()

        data = []

        for chan in range(metric.metric_data.shape[0]):

            chan_range = (
                metric.channel_filter[chan][1] - metric.channel_filter[chan][0]
            )

            for bsln in range(metric.metric_data.shape[1]):

                for pol in range(metric.metric_data.shape[2]):

                    data.append(
                        "{operator},chst={channel},chrg={range},"
                        "bl={baseline},pol={polarization} "
                        "{operand}={vis} {timestamp}".format(
                            operator=metric.operator,
                            channel="%0*d"%(6, metric.channel_filter[chan][0]),
                            range=chan_range,
                            baseline=metric.baseline_filter[bsln],
                            polarization=metric.get_pol_string(pol),
                            operand=metric.operand,
                            vis=metric.metric_data[chan][bsln][pol],
                            timestamp=int(metric.timestep_timestamp*1000)
                        )
                    )
        try:
            self.client.write_points(
                data, database=self.database, time_precision="ms",
                batch_size=metric.metric_data.shape[0], protocol="line"
            )
        except (InfluxDBServerError, InfluxDBClientError) as influx_error:
            warnings.warn(
                "Unable to dispatch metrics to InfluxDB. A number of possible "
                "errors may have been encountered, such as a dropped "
                "connection or attempting to parse invalid data in InfluxDB. "
                f"From InfluxDB: {influx_error}"
            )

        metric.reset_metric()

    @staticmethod
    def load_metrics_from_file(file_path, dimensions) -> np.ndarray:
        """
        Loads 3D matrix from file from a series of 2D matrices
        https://stackoverflow.com/a/3685339
        """
        if not os.path.isfile(file_path):
            raise ValueError("Unable to load specified metric file: \
                {0:s}".format(file_path))

        if dimensions is None:
            raise ValueError("Specified dimensions cannot be None.")

        metric_data = np.loadtxt(file_path)
        return metric_data.reshape(dimensions)

    class Metric:
        """
        Defines a singular metric instance for processing/tracking of desired
        metrics for incoming streams of visibility data (3D numpy array)
        """

        def __init__(
                self, identifier, channel_filter, baseline_filter,
                pol_filter, operand, operator, num_timesteps_to_process = 1,
                start_timestamp = None, end_timestamp = None) -> None:

            # Validate identifier
            if identifier is None or len(identifier) <= 0:
                raise ValueError("New metric identifier cannot be empty")

            # Validate filter lists
            filters = (channel_filter, baseline_filter, pol_filter)
            if any(fil is None or len(fil) <= 0 for fil in filters):
                raise ValueError("Channel/Baseline/Pol filters cannot \
                                  be empty")

            # Validate supplied operator/operand
            ops_set = set(VALID_OPERANDS + VALID_OPERATORS)
            if not set([operator, operand]) <= ops_set:
                raise ValueError(f"Invalid operator or operand provided "
                                 f"when adding new metric instance. Operator: "
                                 f"{operator}, operand: {operand}.")

            self.identifier = identifier
            self.channel_filter = channel_filter
            self.baseline_filter = baseline_filter
            self.pol_filter = pol_filter
            self.num_timesteps_to_process = num_timesteps_to_process
            self.timesteps_processed = 0
            self.operand = operand.casefold()
            self.operator = operator.casefold()
            self.start_timestamp = start_timestamp
            self.end_timestamp = end_timestamp
            self.timestep_timestamp = 0

            self.dimensions = (
                self.channel_filter.shape[0],
                self.baseline_filter.shape[0],
                self.pol_filter.shape[0]
            )

            self.allocate_metric_buffer()

            # user did not define during initialization
            if start_timestamp is None:
                self.start_timestamp = time.time()

            if end_timestamp is None:
                # defaults to 1 hour after (to be moved to config file)
                self.end_timestamp = self.start_timestamp + (60 * 60)

        def process_payload(self, payload, start_channel, end_channel) -> None:
            """
            Processes incoming payload(s) over time, computing requested
            operation and stores internally until the requested number of
            payloads have been processed. Performs compression across channels,
            and filters (ignores) baselines and pols not included in baseline
            and pol filters.
            """
            if payload is None:
                raise ValueError("Payload cannot be None.")

            chans = (start_channel, end_channel)
            # putting the if statement as multiline results in a syntax error.
            if any(chan is None or chan < 0 for chan in chans) or end_channel < start_channel:
                raise ValueError("Start/end channels cannot be negative, \
                    and end channel must not be smaller than start channel.")

            # For each channel range tuple in chan filter
            chan_num = 0

            for chan_fil_interval in self.channel_filter:

                # For range in channel range tuple
                start_interval = max(chan_fil_interval[0], start_channel)

                end_interval = min(chan_fil_interval[1], end_channel)

                for chan_interval in range(start_interval, end_interval):

                    bl_num = 0

                    # For each baseline not ignored
                    for baseline in self.baseline_filter:

                        pl_num = 0

                        # For each pol not ignored
                        for pol in self.pol_filter:

                            chan = chan_interval-start_channel

                            data = self.get_metric_operand(
                                payload[chan][baseline][pol]
                            )

                            prev = self.metric_data[chan_num][bl_num][pl_num]

                            self.metric_data[chan_num][bl_num][pl_num] = (
                                self.perform_metric_op(prev, data)
                            )

                            pl_num += 1

                        bl_num += 1

                chan_num += 1

        def post_processing(self) -> None:
            """
            Performs post processing of processed payloads before dispatching
            to file (or time-series database). Mean: calculate the overall
            average of the total processed payloads (dependant on filters)
            Variance: calculate the overall variance of the total processed
            payloads (dependant on filters). Min/Max: no post processing needed
            """
            chan_num = 0

            if self.operator == "mean":

                for chan_fil_interval in self.channel_filter:

                    num_samples_per_interval = (
                        chan_fil_interval[1]-chan_fil_interval[0]
                    )

                    total_samples = (
                        num_samples_per_interval * self.timesteps_processed
                    )

                    if total_samples == 0:
                        total_samples = 1

                    self.metric_data[chan_num] /= total_samples
                    chan_num += 1

            elif self.operator == "variance":

                buffer = np.zeros(self.metric_data.shape, dtype=np.double)

                for chan_fil_interval in self.channel_filter:

                    num_samples_per_interval = (
                        chan_fil_interval[1] - chan_fil_interval[0]
                    )

                    total_samples = (
                        num_samples_per_interval * self.timesteps_processed
                    )

                    val_sum_square = (
                        self.metric_data[chan_num].imag / total_samples
                    )

                    val_sum = (
                        (self.metric_data[chan_num].real / total_samples)**2
                    )

                    buffer[chan_num] = val_sum_square - val_sum
                    chan_num += 1

                self.metric_data = buffer
            else: # min or max
                pass

        def reset_metric(self) -> None:
            """
            Resets 3D numpy buffer and resets counters
            """
            self.allocate_metric_buffer()
            self.timesteps_processed = 0
            self.timestep_timestamp = 0

        def allocate_metric_buffer(self) -> None:
            """
            Allocates buffer with suitable type for current metric
            """
            dims = self.dimensions
            if self.operator == "min":
                self.metric_data = np.full(dims, np.inf, np.double)
            elif self.operator == "max":
                self.metric_data = np.full(dims, np.NINF, dtype=np.double)
            elif self.operator == "variance":
                self.metric_data = np.zeros(dims, dtype=np.complex128)
            else: # mean
                self.metric_data = np.zeros(dims, dtype=np.double)

        def perform_metric_op(self, previous_data, payload_data) -> np.double:
            """
            Performs specific metric operation against processed
            payloads so far and the most recent payload received
            """
            if previous_data is None or payload_data is None:
                raise ValueError("Previous/Payload data cannot be None.")

            val = 0.0

            if self.operator == "min":
                val = np.minimum(previous_data, payload_data)
            elif self.operator == "max":
                val = np.maximum(previous_data, payload_data)
            elif self.operator == "mean":
                val = previous_data + payload_data
            else: # variance
                temp_real = previous_data.real + payload_data
                temp_imag = previous_data.imag + payload_data**2
                val = temp_real + temp_imag * 1j

            return val

        def get_metric_operand(self, payload) -> np.double:
            """
            Gets or calculates the desired value for "this" metrics operand
            """
            if payload is None:
                raise ValueError("Data payload cannot be None.")

            val = 0.0

            if self.operand == "real":
                val = payload.real
            elif self.operand == "imaginary":
                val =  payload.imag
            elif self.operand == "amplitude":
                val =  np.linalg.norm(payload) # magnitude
            else: # phase
                val =  np.angle(payload) # arctan(im/re)

            return val

        def get_pol_string(self, pol_index) -> str:
            """
            Converts polarization into baseline string value
            """
            if pol_index is None:
                raise ValueError("Pol index cannot be empty (ie: None)")

            pols = {0:"XX", 1:"XY", 2:"YX", 3:"YY"}

            if not pol_index in pols.keys():
                return "unknown"

            return pols[self.pol_filter[pol_index]]

        def get_basic_metadata(self) -> str:
            """
            Returns dictionary of all metric metadata
            (excluding chan/baseline/pol filters)
            """
            info = {}
            info["identifier"] = self.identifier
            info["operand"] = self.operand
            info["operator"] = self.operator
            info["start_time"] = dt.fromtimestamp(
                self.start_timestamp,
                tz=timezone.utc).strftime("%d/%m/%Y, %H:%M:%S")
            info["end_time"] = dt.fromtimestamp(
                self.end_timestamp,
                tz=timezone.utc).strftime("%d/%m/%Y, %H:%M:%S")
            info["total_timesteps_to_process_per_dispatch"] = (
                self.num_timesteps_to_process
            )
            info["number_of_timesteps_processed"] = self.timesteps_processed
            info["number_of_channel_filter_entries"] = len(self.channel_filter)
            info["number_of_baseline_filter_entries"] = (
                len(self.baseline_filter)
            )
            info["number_of_pol_filter_entries"] = len(self.pol_filter)
            return info

        def get_full_metadata(self) -> str:
            """
            Returns dictionary of all metric metadata
            (including chan/baseline/pol filters)
            """
            info = self.get_basic_metadata()
            info["channel_filters"] = self.channel_filter.tolist()
            info["baseline_filters"] = self.baseline_filter.tolist()
            info["pol_filters"] = self.pol_filter.tolist()
            return info
