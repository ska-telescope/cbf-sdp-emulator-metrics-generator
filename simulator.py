#!/usr/bin/env python3

# Copyright 2020 Adam Campbell, Seth Hall, Andrew Ensor
# Copyright 2020 High Performance Computing Research Laboratory, Auckland University of Technology (AUT)

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:

# 1. Redistributions of source code must retain the above copyright notice,
# this list of conditions and the following disclaimer.

# 2. Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution.

# 3. Neither the name of the copyright holder nor the names of its
# contributors may be used to endorse or promote products derived from this
# software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import numpy as np
import time
import metric_generator as gen

# Currently available options for metric generation
valid_operands = ["real", "imaginary", "amplitude", "phase"]
valid_operators = ["min", "max", "mean", "variance"]

#using gleam data from file if fake data false
fake_data = False

# Parameters for simulating send/receive
num_payloads_to_simulate = 3
# NOTE: For GLEAM large test data, valuees are 4, 130816, 1
num_channels = 4
num_baselines = 130816
num_pols = 1

# Channel filters - what channels do we want to keep and/or compress
channel_filter = np.zeros((num_channels, 2), dtype=np.uint32)
for cf in range(num_channels):
    channel_filter[cf] = (cf, cf + 1) # inclusive / exclusive range

# Baseline filters - which baselines do we want to keep
baseline_filter = np.arange(0, 10)

# Polarization filters - which pols do we want to keep
pol_filter = np.arange(0, num_pols)

# New instance of generator, and new instance of metric for current simulation settings
#generator = gen.MetricGenerator(num_channels, num_baselines, num_pols, False)
generator = gen.MetricGenerator(num_channels, num_baselines, num_pols)
generator.parse_yaml_output_config("metric_config.yaml")
generator.add_new_metric("test", channel_filter, baseline_filter, pol_filter, "amplitude", "mean", 1)

payload = np.zeros((num_channels, num_baselines, num_pols), dtype=np.complex128)
# Simulate sending and parsing n payloads of random "visibility" data
for i in range(num_payloads_to_simulate):

	print("Processing payload %d" % (i))

	payload = np.zeros((num_channels, num_baselines, num_pols), dtype=np.complex128)

	if fake_data:
		for x, y, z in np.ndindex(num_channels, num_baselines, num_pols):
			payload[x][y][z] = ((x + 1.0) + (y * 0.1)) + (z * 0.01) * 1j
	else:
		payload = np.loadtxt("GLEAM_large_timesteps/timestep_" + str(i % num_payloads_to_simulate) + ".txt", dtype=np.complex_)

		payload = payload.reshape(num_channels, num_baselines, num_pols)

	generator.process_payload(payload, 0, num_channels, time.time())

#flush the metrics for processing
generator.process_payload(payload,0,num_channels,time.time())
# Metric being tracked will be sent to file after num_payloads_to_simulate of cycles
