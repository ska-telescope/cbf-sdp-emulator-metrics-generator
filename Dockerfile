
# Base this file from the CBD-SDP-Emulator ver 1.2
FROM nexus.engageska-portugal.pt/ska-docker/cbf_sdp_emulator:1.2 

RUN apt-get update && apt-get install -y \
    python3-distutils \
    python3-setuptools \
 && rm -rf /var/lib/apt/lists/*

# Install additional software (python packages)
RUN pip install influxdb
RUN pip install pyyaml

# Bring over the source code
RUN mkdir -p /metric_generator
COPY metric_generator.py cbf_sdp_consumer.py /metric_generator/
ENV PYTHONPATH "${PYTHONPATH}:/metric_generator"
