import asyncio
import concurrent.futures
import logging

import metric_generator
import numpy as np



logger = logging.getLogger(__name__)

class consumer(object):

    def __init__(self, config, tm):
        self.tm = tm
        self.gen = metric_generator.MetricGenerator(tm.num_channels,
                tm.num_baselines, tm.num_pols)
        if 'metrics_db_connection' in config:
            logger.info("Setting up connection to InfluxDB: %r",
            dict(config['metrics_db_connection']))
            if not self.gen.setup_db_connection(config['metrics_db_connection']):
                logger.warning('Could not connect to InfluxDB')
            else:
                self.gen.to_database = True

        # Setup filters
        channel_filter = np.zeros((tm.num_channels, 2), dtype=np.uint32)
        for cf in range(tm.num_channels):
            channel_filter[cf] = (cf, cf + 1) # inclusive / exclusive range
        baseline_filter = np.arange(0, tm.num_baselines)
        pol_filter = np.arange(0, tm.num_pols)

        # channel_filter = np.array([(0, tm.num_channels)])
        # baseline_filter = np.array([0, 2, 4, 6, 8])
        # pol_filter = np.array([0])

        self.gen.add_new_metric("test", channel_filter, baseline_filter, pol_filter, "amplitude", "mean", 1)
        # self.gen.add_new_metric("test2", channel_filter, baseline_filter, pol_filter, "amplitude", "min", 1)
        # self.gen.add_new_metric("test3", channel_filter, baseline_filter, pol_filter, "amplitude", "max", 1)
        # self.gen.add_new_metric("test4", channel_filter, baseline_filter, pol_filter, "amplitude", "variance", 1)

        # Use a threadpool executor for long-running tasks
        self.executor = concurrent.futures.ThreadPoolExecutor(max_workers=1)

    async def consume(self, payload):
        await asyncio.get_event_loop().run_in_executor(self.executor,
                self.gen.process_payload, payload.visibilities,
                payload.channel_id, payload.channel_id + payload.channel_count,
                payload.unix_time)
