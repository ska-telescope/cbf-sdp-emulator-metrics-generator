# CBF-SDP Emulator Metrics Generator (Prototype)
CBF-SDP Emulator Metrics Generator is a Python based tool for generating custom metrics/statistics of streaming observational visibility data. Its purpose is to allow users to define one (or more) concurrent metrics to be measured, as visibilities are streamed via some method of data ingest (ie: SPEAD), which are periodically deposited into some unit of storage (ie: time-series database). It features filtering across channels, baselines, and polarisation, and tracking of multiple of metric instances at any time. It is intended for use as a ***prototype*** metric generator for the [CBF-SDP Emulator](https://gitlab.com/ska-telescope/cbf-sdp-emulator).

---
## Table of Contents

* [Prerequisites](#prerequisites)
* [Usage](#usage)
* [Examples](#examples)
* [Integration](#integration)
* [Testing](#testing)
* [License](#license)

---
## Prerequisites

#### Time-series Database and Visualization
The current implementation uses InfluxDB for storing the metrics in a time series database. InfluxDB can be installed as a Docker Image, for Mac OS X, or Linux following instructions at [InfluxDB Installation](https://portal.influxdata.com/downloads/). Older versions such as 1.8.3 can also be installed on Windows following the instructions found here: [Windows InfluxDB Installation](https://devconnected.com/how-to-install-influxdb-on-windows-in-2019/)

Once installed an InfluxDB database should be created (named for example “gleam”). From the command line:
```bash
$ influx -precision rfc3339
$ CREATE DATABASE gleam
```
No database schema need be specified when using InfluxDB. The metrics stored in InfluxDB can be visualised using any visualisation tool, such as Grafana which is available from [Grafana Installation](https://grafana.com/grafana/download) for Linux, Windows, Mac, or ARM, or as a Docker container. There are some sample Grafana dashboards available within this repository that can be imported (including one for visualising the metrics generated for the GLEAM dataset)

---
## Usage

The general idea behind using the `Metric Generator` is to create a new instance of the metric generator object at the top level of where metrics are to be measured/tracked - assuming your metric generator will have access to incoming 3D visibility data (we use the term `payload` to describe this block of data). It is assumed that each payload is a three-dimensional numpy array of complex values, in the order of `payload[channels][baselines][polarizations]` for your observation.

One `Metric Generator` instance is expected to internally _spawn_ off multiple instances of the `Metric` class, each of which will track desired metric data of your choice. The available operands to track (at the moment) are the ```real, imaginary, amplitude, or phase``` components of the payload. The available operation to perform against the data (also at the moment) are ```min, max, mean, variance```. 

When creating a new instance of the ```Metric``` class, your will provide a unique identifier string (for management), as well as which operand and operator you desire. You will also need to supply filters to restrict processing against only the data you are interested in (ie: specific channels/bandwidths, baselines, or polarisations). Furthermore you will need to specify how many payloads you wish to have processed before the metric will be dispatched (to file, to time-series database, etc.). Lastly, you have the option to specify a start time and end time for the metrics "life span". 

**Note that once a metric has expired, it is permanently removed from the metric generator processing queue. It does not persist in memory, and is not shifted over to some form of archival system. This is an assumption made at this time. However, it could be changed if there is enough interest in maintaining historical records.**

It is expected that `chan_filter` will be a list of integer pairs, where each pair represents the range of channels to compress into a single value. The pair range follows the convention of (start_channel, end_channel), that is, inclusive and exclusive. The `baseline filter` and `polarization filter` are simple lists of integers, which state which baselines/polarisations to **include** in metric processing. 

A brief example is seen below:

```python
import time
import numpy as np
import metric_generator as gen

# Specify requirements of the metric to be added for processing
id               = "my_custom_metric_1234"
chan_fil         = np.array([(a, b), (c, d), ..., (y, z)])
baseline_fil     = np.arange(0, num_baselines)
pol_fil          = np.arange(0, num_pols)
loads_to_process = n
operand          = "operand"  # ie: "real", "imaginary", "amplitude", or "phase"
operator         = "operator" # ie: "min", "max", "mean", or "variance"
start_time       = time.time()
end_time         = start_time + (60 * 60) # one hour later

# New instance of the generator with total number of channels, baselines and polarizations for the observavation. Note by default elapsed metrics will be output to a file. Spawn off new metric to process over with required filters of interest
g = gen.MetricGenerator(num_channels, num_baselines, num_pols)
g.add_new_metric(id, chan_fil, baseline_fil, pol_fil, operand, operator, loads_to_process,
    start_time, end_time) # <= optional params

# ...
# Data ingest logic here (ie: SPEAD receiver set up and receiving, etc.)
# ...

# Assumes we have a payload of visibility data (3D numpy array [complex128]), so lets process it...
payload = ... # 3D visibility data from ingest
g.process_payload(payload, start_channel, end_channel, current_timestamp)
```



Realistically, `process_payload` will be called for each received payload of visibility data, which will be processed by every instance of `Metric. ` Either the full payload can be specified at once by setting the `start_channel` and `end_channel` to the full range of channels the metric generator is monitoring or alternatively each payload can be broken down by using a channel sub-range. The Metric Generator will not count this as a full payload accumulation until the `current_timestamp` differs from the previous call to `process_payload`. We did this as some of the  [CBF-SDP-Emulator](https://gitlab.com/ska-telescope/cbf-sdp-emulator) examples had visibilities separated into channels across several SPEAD heaps. Further instances of metric can easily be added at any time to the running instance of `Metric Generator`, so long as you provide your unique identifier, filters, number of payloads to process per dispatch, a valid operand, and a valid operator.

#### Manipulation of Existing Metrics 
It is now possible to update or remove metric instances, or to query the metric generator about the metrics currently loaded for processing. Below describes the available functionality and brief demonstrations of how to use it.

##### Adding a new metric
This has already been covered above, please refer to the [Usage](#usage) section.

##### Updating an existing metric
Assuming we already have an active metric generator instance, with queued metrics already bound to the generator, and is processing time-step payloads, metrics can be updated at user discretion. Below demonstrates the updating of an existing metric to modify which polarisation we are interested in measuring by providing a new pol filter.
```bash
# Assumes we know the identifier for the metric we are interested in
id = "my_custom_id_1234" # we can determine all metric IDs using get_list_of_metrics()
# Setting up a new pol filter list
pol_fil = [1, 3]
# Assumes we have a metric generator instance 'g'
if g.update_existing_metric(id, pol_filter):
    # success!
else:
    # Fail, as the id specified didnt match an existing metric
```
Behind the scenes, the old metric instance (known by its id) is updated to reflect the new polarisation filter specified. It is important to note that any partially processed data held by the updated metric is discarded. All arguments passed to the `update_existing_metric` method are the same as those passed to the `add_new_metric` method, except they are all optional. This allows for a more specific control over metrics from a user perspective.

It is important to note that if the user does not provide a new (or old) start/end timestamp when updating a metric, then the updated metrics timestamps will "reset". Start time will be "now", and end time will be one hour from "now" (to be moved to a configuration file in a future update).

##### Deleting an existing metric
If a user decides they wish to remove an existing metric they are no longer interested in, it can simply be removed from processing. This is simple enough to do, assuming you know the id of the metric you want to destroy. The following demonstrates such a case:
```bash
# Assumes we know the identifier for the metric we are interested in
id = "my_custom_id_1234" # we can determine all metric IDs using get_list_of_metrics()
# Assumes we have a metric generator instance 'g'
if g.remove_existing_metric(id):
    # success!
else:
    # Fail, as the id specified didnt match an existing metric
```
##### Requesting basic information about all active metrics
If someone wishes to know what metrics are currently active in the generator, and basic information about their respective attributes, this can be requested from the generator. At present, it returns all information in the form of a JSON string, as follows...

```bash
# Assuming we have an active instance of the generator 'g', with several active metrics...
result = g.get_list_of_metrics()
# JSON formatted results below for post-processing by end user
# Start/end times are based on UTC timestamps 
[
   {
      "identifier":"Alice_123",
      "operand":"phase",
      "operator":"mean",
      "start_time":"2020-09-17 15:38:18",
      "end_time":"2020-09-17 16:38:18",
      "total_timesteps_to_process_per_dispatch":3,
      "number_of_timesteps_processed":2,
      "number_of_channel_filter_entries":4,
      "number_of_baseline_filter_entries":10,
      "number_of_pol_filter_entries":1
   },
   {
      "identifier":"Bob_456",
      "operand":"amplitude",
      "operator":"variance",
      "start_time":"2020-09-17 15:38:18",
      "end_time":"2020-09-17 16:38:18",
      "total_timesteps_to_process_per_dispatch":10,
      "number_of_timesteps_processed":7,
      "number_of_channel_filter_entries":40,
      "number_of_baseline_filter_entries":10,
      "number_of_pol_filter_entries":4
   }
]
```
##### Requesting more in-depth information about a specific metric
If one wishes to know more about a specific metric instance, further information can be polled from the metric generator if you have a valid identifier...
```bash
# Assuming we have an active instance of the generator 'g', with several active metrics...
result = g.get_metric_config("Alice_123")
# JSON formatted results below for post-processing by end user
# Start/end times are based on UTC timestamps
{
   "identifier":"Alice_123",
   "operand":"phase",
   "operator":"mean",
   "start_time":"17/09/2020, 04:01:07",
   "end_time":"17/09/2020, 05:01:07",
   "total_timesteps_to_process_per_dispatch":3,
   "number_of_timesteps_processed":1,
   "number_of_channel_filter_entries":4,
   "number_of_baseline_filter_entries":10,
   "number_of_pol_filter_entries":1,
   "channel_filters":[
      [0,1],
      [1,2],
      [2,3],
      [3,4]
   ],
   "baseline_filters":[
      0, 1, 2, 3, 4, 5, 6, 7, 8, 9
   ],
   "pol_filters":[
      0
   ]
}
```
The above example tells us that the metric identified as `Alice_123` is currently only measuring for 4 uncompressed channels, for 10 different baselines, and only across 1 polarisation. Once again, this result is in JSON format, and is assumed an end user has some form of post-processing to utilise the information.

#### Output metrics to external source
By default any metrics that are accumulated are output to file, this file has a path and a prefix which then concatenated with the operator and operand of the metric. The default prefix and output folder is shown below:
```bash
self.output_file_path = "test_data/metric_output_"
```
The metric generator allows a YAML configuration file to determine where accumulated metrics should be output to, by either setting new file destination or connection parameters to a time series database. You can use the following method which takes in the path and file name to a YAML configuration file. This should be called straight after the creation of the metric generator if a non-default output of metrics should be used. 

```bash
g = gen.MetricGenerator(num_channels, num_baselines, num_pols)
g.parse_yaml_output_config("path_to/metric_config.yaml")
```
A full example of the `metric_config.yaml` is shown below:
```bash
---
output_to_database: false
# database parameters
database_name: "gleam"
host_name: "localhost"
port_number: 8086

# file parameters
output_file_path: "test_data/metric_output_"
```

##### Custom output file using YAML
If a new file path and prefix should be set for output metrics you want to be sure to set the `output_to_database` flag to false and set the new `output_file_path` string in the YAML configuration file.

##### Custom output to Influx time series database using YAML
If output metrics should be sent to an Influx time series database then the `output_to_database` flag should be set to true. In addition the `database_name`, `host_name` and `port_number` strings need to be set to an actively running an Influx database server. Note that the python3 package `influxdb` is required to be installed when using the metric generator.

**TODO:** secure connections with username and password tokens for HTTPS connection:

---
## Examples
##### Measuring the real average of 10 payloads (all channels, baselines, pols)
Assuming we have the following configuration for our observation (only for demonstration purposes):
```bash
num_payloads_to_simulate = 10
num_channels = 5
num_baselines = 5
num_pols = 4

payload = [[[1.+0.j   1.+0.01j 1.+0.02j 1.+0.03j]
            [1.+0.1j  1.+0.11j 1.+0.12j 1.+0.13j]
            [1.+0.2j  1.+0.21j 1.+0.22j 1.+0.23j]
            [1.+0.3j  1.+0.31j 1.+0.32j 1.+0.33j]
            [1.+0.4j  1.+0.41j 1.+0.42j 1.+0.43j]]

           [[2.+0.j   2.+0.01j 2.+0.02j 2.+0.03j]
            [2.+0.1j  2.+0.11j 2.+0.12j 2.+0.13j]
            [2.+0.2j  2.+0.21j 2.+0.22j 2.+0.23j]
            [2.+0.3j  2.+0.31j 2.+0.32j 2.+0.33j]
            [2.+0.4j  2.+0.41j 2.+0.42j 2.+0.43j]]

           [[3.+0.j   3.+0.01j 3.+0.02j 3.+0.03j]
            [3.+0.1j  3.+0.11j 3.+0.12j 3.+0.13j]
            [3.+0.2j  3.+0.21j 3.+0.22j 3.+0.23j]
            [3.+0.3j  3.+0.31j 3.+0.32j 3.+0.33j]
            [3.+0.4j  3.+0.41j 3.+0.42j 3.+0.43j]]

           [[4.+0.j   4.+0.01j 4.+0.02j 4.+0.03j]
            [4.+0.1j  4.+0.11j 4.+0.12j 4.+0.13j]
            [4.+0.2j  4.+0.21j 4.+0.22j 4.+0.23j]
            [4.+0.3j  4.+0.31j 4.+0.32j 4.+0.33j]
            [4.+0.4j  4.+0.41j 4.+0.42j 4.+0.43j]]
            
           [[5.+0.j   5.+0.01j 5.+0.02j 5.+0.03j]
            [5.+0.1j  5.+0.11j 5.+0.12j 5.+0.13j]
            [5.+0.2j  5.+0.21j 5.+0.22j 5.+0.23j]
            [5.+0.3j  5.+0.31j 5.+0.32j 5.+0.33j]
            [5.+0.4j  5.+0.41j 5.+0.42j 5.+0.43j]]]
```
The following filters will be generated:
```bash
# Generating an uncompressed list of channel filter pairs
chan_filter = np.zeros((num_channels, 2), dtype=np.uint32)
for cf in range(num_channels):
    chan_filter[cf] = (cf, cf + 1) # inclusive / exclusive range
    
# Generating baseline filter (include all baselines)
baseline_filter = np.arange(0, num_baselines)

# Generating pol filter (include all pols)
pol_filter = np.arange(0, num_pols)
    
# Channel filter
[[0 1]
 [1 2]
 [2 3]
 [3 4]
 [4 5]]
 
# Baseline filter
[0 1 2 3 4]
 
# Pols filter
[0 1 2 3]
 
g = gen.MetricGenerator(num_channels, num_baselines, num_pols)
g.add_new_metric("my_id", chan_filter, baseline_filter, pol_filter, "real", "mean", 10)

# for 10 iterations (processing all channels and using current time_stamp)
    g.process_payload(payload, 0, num_channels, time.time())
```

After processing 10 iterations of the same payload (only for demonstration purposes), internally to the metric generator, the metric we registered will "complete", and will be dispatched to a flat file (for now). Peeking into this output file, we can see the overall mean of all real components processed:

```bash
# All channels, all baselines, all polarizations
1.000000 1.000000 1.000000 1.000000
1.000000 1.000000 1.000000 1.000000
1.000000 1.000000 1.000000 1.000000
1.000000 1.000000 1.000000 1.000000
1.000000 1.000000 1.000000 1.000000
# All channels, all baselines, all polarizations
2.000000 2.000000 2.000000 2.000000
2.000000 2.000000 2.000000 2.000000
2.000000 2.000000 2.000000 2.000000
2.000000 2.000000 2.000000 2.000000
2.000000 2.000000 2.000000 2.000000
# All channels, all baselines, all polarizations
3.000000 3.000000 3.000000 3.000000
3.000000 3.000000 3.000000 3.000000
3.000000 3.000000 3.000000 3.000000
3.000000 3.000000 3.000000 3.000000
3.000000 3.000000 3.000000 3.000000
# All channels, all baselines, all polarizations
4.000000 4.000000 4.000000 4.000000
4.000000 4.000000 4.000000 4.000000
4.000000 4.000000 4.000000 4.000000
4.000000 4.000000 4.000000 4.000000
4.000000 4.000000 4.000000 4.000000
# All channels, all baselines, all polarizations
5.000000 5.000000 5.000000 5.000000
5.000000 5.000000 5.000000 5.000000
5.000000 5.000000 5.000000 5.000000
5.000000 5.000000 5.000000 5.000000
5.000000 5.000000 5.000000 5.000000
```
##### Measuring amplitude variance of 10 payloads (compressed channels, reduced baselines, and reduced pols)
It is assumed we are using the same configuration as the above example (i.e. number of channels, etc), including the same payload data, so this will be omitted. However, we will compress several channels, and reduce how many baselines/pols we will target. We will also be tracking variance across the amplitude of visibility data, instead of the mean of the real components.
```bash
# Generating an compressed list of channel filter pairs, ignoring the 3rd channel
chan_fil= np.array([(0, 2), (3, 5)])

# Generating baseline filter (reduced baselines)
baseline_fil = np.array([0, 2, 4])

# Generating pol filter (reduced pols)
pol_fil = np.array([1, 3])
    
# Channel filter
[[0 2]
 [3 5]]
 
# Baseline filter
[0 2 4]
 
# Pols filter
[1 3]
 
g = gen.MetricGenerator(num_channels, num_baselines, num_pols)
g.add_new_metric("my_id", chan_fil, baseline_fil, pol_fil, "amplitude", "variance", 10)

# for 10 iterations (processing all channels and using current time_stamp)
    g.process_payload(payload, 0, num_channels, time.time())
```

After processing 10 iterations of the same payload (also only for demonstration purposes), internally to the metric generator, the metric we registered will "complete", and will be dispatched to a flat file (for now). Peeking into this output file, we can see the amplitude variance of our filtered visibilities over 10 payloads:

```bash
# Channels 0 and 1, only baselines 0, 2, and 4, and only polarizations 1 and 3
0.249988 0.249888
0.244621 0.243578
0.230787 0.229044
# Channels 3 and 4, only baselines 0, 2, and 4, and only polarizations 1 and 3
0.249999 0.249989
0.249450 0.249341
0.247916 0.247710
```
-------
## Integration

It was always intended for the Metric Generator to be integrated with the [CBF-SDP-Emulator](https://gitlab.com/ska-telescope/cbf-sdp-emulator) project. This has been performed by means of "dynamic" integration using containerization; specifically with the help of Docker. A Dockerfile has been created at the root of the project, which uses the CBF-SDP-Emulator as a base, and sees the Metric Generator copied into the container at build time. The CBF-SDP-Emulator allows for custom consumer objects, so the Metric Generator is encapsulated as a custom consumer in the file `cbf_sdp_consumer.py`.
#### Configuring for execution
A Makefile is used to automate some of the overhead needed to build and run the integrated system via Docker. At present, some of the arguments passed through to the container at runtime are defined as variables within the Makefile. Below is a sample of these variables which should be configured to suit your needs:
```Makefile
DATASET_FOLDER=datasets # folder containing measurement sets
DATASET=sim-vis.ms      # name of measurement set to be processed
DATABASE_NAME=gleam     # name of the database to receive dispatched metrics
HOSTNAME=localhost      # IP for machine with InfluxDB installation
PORT_NUMBER=8086        # port for InfluxDB
TIMESTEPS=133           # Number of timesteps for dataset
CHANNELS_PER_STREAM=4   # Number of channels to send/recv per SPEAD2 send/recv stream
```
At present, to add metrics for processing, one needs to manually insert them into the `cbf_sdp_consumer.py` file. We have developed suitable functions for interacting with metrics at runtime (add/remove/update/query). However, further work is needed to call these functions externally (ie: TANGO interface). Therefore, you should insert your required metrics to be tracked before performing the build stage in the next section. An example of adding a custom metric is seen below (taken from the **__ init __** function of the `cbf_sdp_consumer.py`:
```python
# Assumes a valid telescope manager instance (tm) from the CBF-SDP-Emulator
# Setting up chan/baseline/pol filters for sample metric
channel_filter = np.zeros((tm.num_channels, 2), dtype=np.uint32)
for cf in range(tm.num_channels):
    channel_filter[cf] = (cf, cf + 1) # inclusive / exclusive range
baseline_filter = np.arange(0, tm.num_baselines)
pol_filter = np.arange(0, tm.num_pols)
# Adding instance for tracking
self.gen.add_new_metric("test", channel_filter, baseline_filter, pol_filter, "amplitude", "mean", 1)
```
#### Building the image
Building the Docker image is relatively straight forward, once again using a Makefile command:
```bash
$ make image
# ... build process omitted ...
$ docker images
# REPOSITORY                         TAG     IMAGE ID        CREATED         SIZE
# cbf-sdp-emulator-metric-generator  latest  <some-id-here>  19 seconds ago  370MB
```
#### Executing the CBF-SDP-Emulator/Metric Generator via Docker

As the CBF-SDP-Emulator is designed to send and receive measurement set data via SPEAD2 heaps, with the Metric Generator on the receive end as a custom consumer, two Docker containers should be executed via separate terminals. Note that the receiver should be started before the sender as the UDP protocol is used for SPEAD. Once again, the Makefile automates this process:
```bash
$ make recv-demo
# docker run -v /~/cbf-sdp-emulator-metrics-generator/datasets:/datasets \
# --rm --name metric_generator_recv cbf-sdp-emulator-metric-generator \
# emu-recv -o \
# reception.datamodel=/datasets/sim-vis.ms -o \
# reception.consumer=cbf_sdp_consumer.consumer -o \
# transmission.channels_per_stream=4 -o \
# reception.ring_heaps=133 -o \
# metrics_db_connection.database_name=gleam -o \
# metrics_db_connection.host_name=localhost  -o \
# metrics_db_connection.port_number=8086

# INFO|MainThread|__init__|utils.py#45||Attempting to build model from /datasets/sim-vis.ms
# INFO|MainThread|__init__|cbf_sdp_consumer.py#20||Setting up connection to InfluxDB: {'database_name': 'gleam', 'host_name': 'localhost', 'port_number': '8086'}
# INFO|MainThread|__init__|spead2_receivers.py#84||Creating stream with 1 UDP readers to receive data for 4 channels
# INFO|MainThread|_setup_streams|spead2_receivers.py#98||Started udp_reader on port 41000
```
Then in a separate terminal, initiate a sender...
```bash
$ make send-demo
# docker run -v /~/cbf-sdp-emulator-metrics-generator/datasets:/datasets \
# --rm --name metric_generator_send \
# --link metric_generator_recv:metric_generator_recv cbf-sdp-emulator-metric-generator \
# emu-send /datasets/sim-vis.ms -o \
# transmission.channels_per_stream=4 -o \
# transmission.target_host=metric_generator_recv

# INFO|MainThread|packetise|packetiser.py#49||AUTOs present
# INFO|MainThread|packetise|packetiser.py#58||no. stations        : 4
# INFO|MainThread|packetise|packetiser.py#59||no. baselines       : 10
# INFO|MainThread|packetise|packetiser.py#60||no. channels        : 4
# INFO|MainThread|packetise|packetiser.py#61||first channel       : 0
# INFO|MainThread|packetise|packetiser.py#62||channels per stream : 4 (0 == all)
# INFO|MainThread|__init__|spead2_transmitters.py#187||Creating StreamConfig with max_packet_size=1472
# INFO|MainThread|_create_streams|spead2_transmitters.py#213||Creating 1 spead2 streams to send data for 4 channels
# INFO|MainThread|_create_streams|spead2_transmitters.py#229||Sending to metric_generator_recv:41000
# INFO|MainThread|packetise|packetiser.py#87||Sent 0.448 MB in 0.190 sec (2.360 MB/sec)
```
Back on the receiver end, data silently streams in, and metrics are send to InfluxDB...
```bash
# ... setup omitted (see earlier make-recv output) ...
# INFO|ThreadPoolExecutor-0_0|dispatch_metric_db|metric_generator.py#356||Dispatching metric test to InfluxDB
# ... additional dispatch notices omitted ...
# INFO|ThreadPoolExecutor-0_0|dispatch_metric_db|metric_generator.py#356||Dispatching metric test to InfluxDB
# INFO|MainThread|run|spead2_receivers.py#107||Received 133 heaps
```

The sender will automatically terminate once sending is complete, and the receiver will automatically terminate once all SPEAD2 heaps have been received and metric dispatching is completed. Note that there is the potential for some heaps to be dropped if a complete heap cannot be put together on the recieve end. This is likely when huge volumes of data are transmitted, or poor filtering is performed for metrics on the receiving end.

InfluxDB can then be manually queried for metrics, assuming they have been successfully dispatched. Alternatively, a Grafana dashboard can be used to visually view the data if a dashboard has been produced.
```bash
$ influx
# Connected to http://localhost:8086 version 1.8.1
# InfluxDB shell version: 1.8.1
> use gleam
# Using database gleam
> select * from mean
# time                amplitude          bl chrg  chst   pol
# ----                ---------          -- ----  ----   ---
# 1435097164229000000 7.849871754646301  0  4     000000 XX
# 1435097164229000000 6.630937278270721  8  4     000000 XX
# 1435097164229000000 3.98969966173172   4  4     000000 XX
# 1435097164229000000 6.60995489358902   2  4     000000 XX
# ...
# 1435097165129000000 8.172959715127945  4  4     000000 XX
```


-------
## Testing

Running tests is relatively straight forward. Assuming the test data is present (inside `test_data` folder at root), you can simply run the following command via terminal, and should get a similar output as below (aside from maybe different versions of python/modules):
```bash
# Assuming you are at the root folder of the software
$ pytest
================================ test session starts ===================================
platform linux -- Python 3.6.9, pytest-5.2.1, py-1.8.0, pluggy-0.13.0
rootdir: /Desktop/HPC/Official_SKA_Projects/cbf-sdp-emulator-metrics-generator
plugins: asdf-2.4.2, Faker-4.1.3, doctestplus-0.4.0, arraydiff-0.3, remotedata-0.3.2, openfiles-0.4.0
collected 38 items                                                                                                                 
test_metric_generator.py ..........................                               [100%]
================================== 38 passed in 0.34s ================================== 
```
If testing fails, either you haven't got the test data files present under the `test_data` folder, or you have broken the source code logic.

#### Code Coverage
Assuming you have the [Coverage](https://coverage.readthedocs.io/) Python tool installed, code coverage can be easily analysed using the following command:
```bash
# Assuming you are at the root folder of the software
$ coverage run -m pytest
# Then query for percentage of coverage of the metric generator tool
$ coverage report metric_generator.py
Name                  Stmts   Miss  Cover
-----------------------------------------
metric_generator.py     306     23    92%
```
---
## License
[BSD 3-Clause “New” or “Revised” License](https://choosealicense.com/licenses/bsd-3-clause/)
